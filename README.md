A current version of the proposal PDF can be found at the link below. It automatically updated when this repo updates, so it will always reflect the content in the repo.

[https://ufoi.gitlab.io/constitution/united_federation_of_instances_proposal.pdf](https://ufoi.gitlab.io/constitution/united_federation_of_instances_proposal.pdf)

The current versionn of the bylaws can be found at the following link and is likewise automatically updated:

[https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf](https://ufoi.gitlab.io/constitution/united_federation_of_instances_bylaws.pdf)

If you want access to create a merge request please just request access, All requests will be approved. Feel free to discuss changes, concerns or anything else in the issues section.

Find us on the fediverse either at the group account here: @ufoi@a.gup.pe or by searching for the #UFoI hashtag.

Find us on Matrix chat at the following room: #UFoI:matrix.org
