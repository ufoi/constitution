# Contributors

Below are all the people who helped contribute to the drafting of the UFI constitution. By default we list their Fediverse handles. Anyone who has contributed is welcome to have their full name and/or email added as well.

* Jeffrey Phillips Freeman (@freemo@qoto.org)
* @timezoneless@mstdn.social 
* @floppy@fosstodon.org 
* @skanman@qoto.org
* @john@sauropods.win 
* @barefootstache@qoto.org
* @stevenclyman@noc.social 
* @dashrandom@kopiti.am 
* @Romaq@qoto.org
* @ichoran@qoto.org
* @AlanOutback@qoto.org
* @tsomof@social.freetalklive.com 
* @aebrockwell@qoto.org
* @Ryle@awoo.fyi
* @tatzelbrumm@qoto.org
* @Gaythia@qoto.org
* @realcaseyrollins@social.teci.world 
* @trinsec@qoto.org
* @khird@qoto.org
* @darnell@one.darnell.one 
* @jq@fosstodon.org 
* Phillip Winn (@pwinn@qoto.org)
* @Dennis1212@mas.to
* @Tonic1@mas.to
* Alecu Ștefan-Iulian (@alecui@qoto.org)
